import Phaser from "phaser";

import constants from "./config/constants";
import GameScene from "./scenes/game";

const config = {
  type: Phaser.WEBGL,
  width: 640,
  height: 640,
  backgroundColor: "black",
  physics: {
    default: "arcade",
    arcade: {
      Gravity: {
        x: 0,
        y: 0
      }
    }
  },
  scene: [
    GameScene
  ],
  pixelArt: true,
  roundPixels: true
};

// eslint-disable-next-line no-new
new Phaser.Game(config);

if (module.hot) {
  module.hot.accept(() => {});

  module.hot.dispose(() => {
    window.location.reload();
  });
}
